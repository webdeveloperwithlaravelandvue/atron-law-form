@extends('admin.includes.admin_design')


@section('site_title')
  Add New Page - {{ $themes->website_name }}
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">


        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        <h5 class="mb-0">Add New Page</h5>
                        <hr>
                        <div class="card shadow-none border">

                            @include('admin.includes._message')

                            <div class="card-body">
                                <form class="row g-3" method="post" action="{{ route('page.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-6">
                                        <label for="page_name" class="form-label">Page Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" value="{{ old('page_name') }}" name="page_name" id="page_name">
                                    </div>

                                    <div class="col-6">
                                        <label for="slug" class="form-label">Page Slug <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" value="{{ old('slug') }}" name="slug" id="slug">
                                    </div>

                                    <div class="col-6">
                                        <label for="thumbnail_image" class="form-label">Page Image</label>
                                        <input type="file" class="form-control"  name="thumbnail_image" id="thumbnail_image" accept="image/*" onchange="readURL(this)">
                                    </div>

                                    <div class="col-6"></div>

                                    <img id="one" src="" alt="" style="width: 300px !important;">

                                    <h5>SEO Information</h5>

                                    <div class="col-6">
                                        <label for="seo_title" class="form-label">SEO Title </label>
                                        <input type="text" class="form-control" value="{{ old('seo_title') }}" name="seo_title" id="seo_title">
                                    </div>

                                    <div class="col-6">
                                        <label for="seo_subtitle" class="form-label">SEO Sub Title </label>
                                        <input type="text" class="form-control" value="{{ old('seo_subtitle') }}" name="seo_subtitle" id="seo_subtitle">
                                    </div>

                                    <div class="col-12">
                                        <label for="seo_keywords" class="form-label">SEO Keywords </label>
                                        <input type="text" class="form-control" value="{{ old('seo_keywords') }}" name="seo_keywords" id="seo_keywords">
                                    </div>

                                    <div class="col-12">
                                        <label for="seo_description" class="form-label">SEO Description </label>
                                        <input type="text" class="form-control" value="{{ old('seo_description') }}" name="seo_description" id="seo_description">
                                    </div>

                                    <div class="text-start">
                                        <button type="submit" class="btn btn-success px-4">Save Information</button>
                                    </div>

                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </div><!--end row-->

    </main>
    <!--end page main-->
@endsection

@section('js')


    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#details').summernote({
                height: 100
            });
        });
    </script>

    <script type="text/javascript">
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#one").attr('src', e.target.result).width(300);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>


@endsection
