

<a href="{{ $url_edit }}" class="btn btn-primary btn-sm">
    <i class="bx bx-edit-alt"></i>
</a>

<a href="{{ $url_delete }}" class="btn btn-danger btn-sm btn-delete" rel="{{ $model->id }}" rel1="delete-page">
    <i class="bx bx-trash-alt"></i>
</a>
