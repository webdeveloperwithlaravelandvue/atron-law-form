<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/images/favicon-32x32.png" type="image/png" />
    <!-- Bootstrap CSS -->
    <link href="{{ asset('public/adminpanel/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/adminpanel/assets/css/bootstrap-extended.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/adminpanel/assets/css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/adminpanel/assets/css/icons.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <!-- loader-->
    <link href="{{ asset('public/adminpanel/assets/css/pace.min.css') }}" rel="stylesheet" />

    <title>Reset Password</title>
</head>

<body>

<!--start wrapper-->
<div class="wrapper">

    <!--start content-->
    <main class="authentication-content">
        <div class="container-fluid">
            <div class="authentication-card">
                <div class="card shadow rounded-0 overflow-hidden">
                    <div class="row g-0">
                        <div class="col-lg-6 d-flex align-items-center justify-content-center border-end">
                            <img src="{{ asset('public/adminpanel/assets/images/error/forgot-password-frent-img.jpg') }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6">
                            <div class="card-body p-4 p-sm-5">
                                <h5 class="card-title">Reset Your Password</h5>
                                <form class="form-body" method="post" action="{{ route('adminResetPassword') }}">
                                    @csrf
                                    <div class="row g-3">

                                        @include('admin.includes._message')


                                        <div class="col-12">
                                            <label for="email" class="form-label">E-Mail Address</label>
                                            <div class="ms-auto position-relative">
                                                <div class="position-absolute top-50 translate-middle-y search-icon px-3"><i class="bi bi-lock-fill"></i></div>
                                                <input type="email" class="form-control radius-30 ps-5" id="email" name="email" placeholder="Enter E-Mail Address">
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="d-grid gap-3">
                                                <button type="submit" class="btn btn-primary radius-30">Reset Password</button>
                                                <a href="{{ route('adminLogin') }}" class="btn btn-light radius-30">Back to Login</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!--end page main-->

</div>
<!--end wrapper-->


<!--plugins-->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/pace.min.js"></script>


</body>

</html>
