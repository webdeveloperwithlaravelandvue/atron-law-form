<!DOCTYPE html>
<html lang="zxx">

@include('front.includes.head')

<body>
<!-- Preloder Area -->
<div class="preloader">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="lds-hourglass"></div>
        </div>
    </div>
</div>
<!-- End Preloder Area -->

@include('front.includes.header')


@yield('content')


@include('front.includes.footer')

</body>
</html>
