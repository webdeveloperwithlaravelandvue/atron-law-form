<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    // Contact Us
    public function contactUs(){
        $site_name = 'Contact Us';
        $page = Page::where('slug', 'contact-us')->first();
        return view ('front.pages.contact', compact('site_name', 'page'));
    }
}
