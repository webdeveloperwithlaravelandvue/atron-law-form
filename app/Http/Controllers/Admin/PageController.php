<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\Facades\DataTables;

class PageController extends Controller
{
    // Index
    public function index(){
        return view ('admin.pages.index');
    }

    // Add
    public function add(){
        return view ('admin.pages.add');
    }

    // Store
    public function store(Request $request){
        $data = $request->all();
        $rules = [
            'page_name' => 'required|max:255',
        ];
        $customMessages = [
            'page_name.required' => 'Page Name is required',
            'page_name.max' => 'Page must not be more than 255 characters',
        ];
        $this->validate($request, $rules, $customMessages);
        $page = new Page();
        $page->page_name = $data['page_name'];

        if($data['slug'] == null){
           $page->slug = Str::slug($data['page_name']);
        } else {
            $page->slug = $data['slug'];
        }
        $page->seo_title = $data['seo_title'];
        $page->seo_subtitle = $data['seo_subtitle'];
        $page->seo_description = $data['seo_description'];
        $page->seo_keywords = $data['seo_keywords'];

        $random = Str::random(20);
        if($request->hasFile('thumbnail_image')){
            $image_tmp = $request->file('thumbnail_image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $page->thumbnail_image = $filename;
            }
        }

        $page->save();
        $notification = array(
            'alert-type' => 'success',
            'message' => 'Page has been Added Successfully'
        );
        return redirect()->back()->with($notification);
    }


    public function dataTable(){
        $model = Page::orderBy('page_name', 'ASC')->get();
        return DataTables::of($model)
            ->addColumn('action', function ($model){
                return view('admin.pages._actions', [
                    'model' => $model,
                    'url_edit' => route('page.edit', $model->id),
                    'url_delete' => route('page.delete', $model->id),
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }


    // Add
    public function edit($id){
        $page = Page::findOrFail($id);
        return view ('admin.pages.edit', compact('page'));
    }

    // Store
    public function update(Request $request, $id){
        $data = $request->all();
        $rules = [
            'page_name' => 'required|max:255',
        ];
        $customMessages = [
            'page_name.required' => 'Page Name is required',
            'page_name.max' => 'Page must not be more than 255 characters',
        ];
        $this->validate($request, $rules, $customMessages);
        $page = Page::findOrFail($id);
        $page->page_name = $data['page_name'];


        $page->seo_title = $data['seo_title'];
        $page->seo_subtitle = $data['seo_subtitle'];
        $page->seo_description = $data['seo_description'];
        $page->seo_keywords = $data['seo_keywords'];

        $random = Str::random(20);
        if($request->hasFile('thumbnail_image')){
            $image_tmp = $request->file('thumbnail_image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $page->thumbnail_image = $filename;
            }
        }

        $page->save();
        $notification = array(
            'alert-type' => 'success',
            'message' => 'Page has been Updated Successfully'
        );
        return redirect()->back()->with($notification);
    }


    public function delete($id){
        $page = Page::findOrFail($id);
        $page->delete();

        $image_path = 'public/uploads/';
        if (file_exists($image_path.$page->thumbnail_image)){
            unlink($image_path.$page->thumbnail_image);
        }
        $notification = array(
            'alert-type' => 'error',
            'message' => 'Page has been Deleted Successfully'
        );
        return redirect()->back()->with($notification);
    }

}
