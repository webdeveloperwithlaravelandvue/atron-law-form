<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class AdminLoginController extends Controller
{
    // Admin Login
    public function adminLogin(){
        if(Auth::guard('admin')->check()){
            return redirect('/admin/dashboard');
        } else {
            return view ('admin.auth.login');
        }
    }

    public function loginAdmin(Request $request){
        $data = $request->all();

        $rules = [
            'email' => 'required|email|max:255',
            'password' => 'required'
        ];
        $customMessages = [
            'email.required' => 'E-Mail Address is required',
            'email.email' => 'Please enter a valid email address',
            'email.max' => 'You are not allowed to enter more than 255 Characters',
            'password.required' => 'Password is required'
        ];
        $this->validate($request, $rules, $customMessages);


        if (Auth::guard('admin')->attempt(['email' => $data['email'], 'password' => $data['password']])){
            return redirect('/admin/dashboard');
        } else {
            Session::flash('error_message', 'Invalid Email or Password');
            return redirect()->back();
        }
    }

    // Admin Dashboard
    public function adminDashboard(){
        return view ('admin.dashboard');
    }

    // Admin Logout
    public function adminLogout(){
        Auth::guard('admin')->logout();
        Session::flash('info_message', 'Logout Successful');
        return redirect('/admin/login');
    }


    // Admin Reset password
    public function adminResetPassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $rules = [
                'email' => 'required|email|max:255',
            ];
            $customMessages = [
                'email.required' => 'E-Mail Address is required',
                'email.email' => 'Please enter a valid email address',
                'email.max' => 'You are not allowed to enter more than 255 Characters',
            ];
            $this->validate($request, $rules, $customMessages);

            $adminCount = Admin::where('email', $data['email'])->count();
            if($adminCount == 0){
                return redirect()->back()->with('error_message', 'E-Mail Address Does Not Exists in our database');
            }

            // Get Admin Details
            $adminDetails = Admin::where('email', $data['email'])->first();
            // Generate Random Password
            $random_password = Str::random(8);
            // Encode Password
            $new_password = bcrypt($random_password);
           // Update Password
            Admin::where('email', $data['email'])->update(['password' => $new_password]);

            // Send Email for the Code
            $email = $data['email'];
            $name = $adminDetails->name;
            $messageData = ['email' => $email, 'password' => $random_password, 'name' => $name];
            Mail::send('emails.forgetpassword', $messageData, function ($message) use ($email){
                $message->to($email)->subject('New Password - Atorn Law Firm');
            });


            Session::flash('error_message', 'Password Has Been Changed. Please Check your Email Address for a new password');
            return redirect()->back();
        }
        return view ('admin.auth.reset');
    }


}
